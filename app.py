# from __future__ import print_function
import os
from flask import Flask, render_template, request, redirect, jsonify
from googleapiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools

# If modifying these scopes, delete the file token.json.
SCOPES = 'https://www.googleapis.com/auth/gmail.readonly'

app = Flask(__name__)

@app.route('/')	
def main():
	"""Shows basic usage of the Gmail API.
	Lists the user's Gmail labels.
	"""
	# The file token.json stores the user's access and refresh tokens, and is
	# created automatically when the authorization flow completes for the first
	# time.
	store = file.Storage('token.json')
	creds = store.get()
	if not creds or creds.invalid:
		flow = client.flow_from_clientsecrets('credentials.json', SCOPES)
		creds = tools.run_flow(flow, store)
	service = build('gmail', 'v1', http=creds.authorize(Http()))

	results = service.users().labels().list(userId='me').execute()
	labels = results.get('labels', [])
	
	if not labels:
		return 'No labels found.'
	else:
		response='<p>Labels:<br><br>'
		for label in labels:
			response=response + label['name']+'<br>'
		print(response)
		return response+'</p>'

@app.route('/shorten/', methods=['POST','GET'])
def shorten():
	return ""

@app.route('/mail')	
def resolve():
	return "asfsd"

if __name__ == '__main__':
	app.jinja_env.auto_reload = True
	app.config['TEMPLATES_AUTO_RELOAD'] = True
	app.run(debug=True)